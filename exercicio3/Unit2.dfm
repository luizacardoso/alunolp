object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 340
  ClientWidth = 313
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 21
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object Label2: TLabel
    Left = 40
    Top = 67
    Width = 44
    Height = 13
    Caption = 'Profiss'#227'o'
  end
  object Label3: TLabel
    Left = 40
    Top = 112
    Width = 23
    Height = 13
    Caption = 'N'#237'vel'
  end
  object Label4: TLabel
    Left = 40
    Top = 158
    Width = 35
    Height = 13
    Caption = 'Ataque'
  end
  object Label5: TLabel
    Left = 40
    Top = 205
    Width = 34
    Height = 13
    Caption = 'Defesa'
  end
  object Label6: TLabel
    Left = 40
    Top = 251
    Width = 20
    Height = 13
    Caption = 'Vida'
  end
  object EDTnome: TEdit
    Left = 40
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object EDTprofissao: TEdit
    Left = 40
    Top = 85
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object EDTnivel: TEdit
    Left = 40
    Top = 131
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object EDTataque: TEdit
    Left = 40
    Top = 176
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object EDTdefesa: TEdit
    Left = 40
    Top = 224
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object EDTvida: TEdit
    Left = 40
    Top = 270
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object BTverificar: TButton
    Left = 200
    Top = 21
    Width = 75
    Height = 25
    Caption = 'Verificar'
    TabOrder = 6
  end
  object BTinserir: TButton
    Left = 200
    Top = 52
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 7
  end
  object ListBox1: TListBox
    Left = 184
    Top = 194
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 8
  end
end
